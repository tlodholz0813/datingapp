import { Injectable } from '@angular/core';
declare let alertify: any;

@Injectable({
  providedIn: 'root'
})
export class AlertifyService {

constructor() { }

// takes a string and a function
  confirm(message: string, okCallBack: () => any) {
    alertify.confirm(message, function(e) {
      // e represents the user click event of the OK button
      if (e) {
        // this is just a wrapper, so the function returned is the one that is provided
        okCallBack();
      } else {}
    });
  }

  // these are all just wrappers so simpler syntax can be used.
  success(message: string) {
    alertify.success(message);
  }

  error(message: string) {
    alertify.error(message);
  }

  warning(message: string) {
    alertify.warning(message);
  }

  message(message: string) {
    alertify.message(message);
  }

}
