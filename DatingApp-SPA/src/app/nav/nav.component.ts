import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  model: any = {};

  constructor(public authService: AuthService, private alertify: AlertifyService, private router: Router ) {}

  ngOnInit() {
  }

  login() {
    this.authService.login(this.model).subscribe(next => {
     this.alertify.success('Login successful.');
     this.router.navigate(['/members']); // uses router to redirect after login.
    }, error => {
      this.alertify.error(error);
    });
  }

  logout() {
    localStorage.removeItem('token');
    this.alertify.message('Logged out.');
    this.router.navigate(['/home']); // uses router to redirect after logout.
  }

  loggedIn() {
    // use auth0 to check token.
    return this.authService.loggedIn();
  }
}
