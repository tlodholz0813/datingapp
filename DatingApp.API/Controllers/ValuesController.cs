﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatingApp.API.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace DatingApp.API.Controllers
{
      [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        //add constructor. this is where dependancy injection is added
        private readonly DataContext _context;
        public ValuesController(DataContext context) 
        {
            _context = context;
        }

       [HttpGet]
        public async Task<IActionResult> GetValues()
        {
            //goes out to the database and returns a list of ALL values from the database
            var values = await _context.Values.ToListAsync();
            return Ok(values);
        }

        // GET http://localhost:5000/api/values/5
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetValue(int id)
        {
            //returns a specific value
            var value = await _context.Values.FirstOrDefaultAsync(x => x.Id == id);
            return Ok(value);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
