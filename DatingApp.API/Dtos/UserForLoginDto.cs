using System.ComponentModel.DataAnnotations;

namespace DatingApp.API.Dtos
{
    //Data Transfer Object - Maps domain model into simpler objects.
    public class UserForLoginDto
    {
        public string Username{ get; set; }

        public string Password{ get; set; }
    }
}